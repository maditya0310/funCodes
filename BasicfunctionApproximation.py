import torch
import torchvision
import torchvision.transforms as transforms

import numpy as np

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F

import torch.optim as optim

import matplotlib.pyplot as plt


class Net(nn.Module):
  def __init__(self, D_in, H, D_out):
    """
    D_in: input dimension
    H: dimension of hidden layer
    D_out: output dimension
    """
    super(Net, self).__init__()
    self.linear1 = nn.Linear(D_in, H) 
    self.linear2 = nn.Linear(H, D_out)
  
  def forward(self, x):


    h_tanh = F.tanh(self.linear1(x))
    y_pred = self.linear2(h_tanh)
    return y_pred

def example2():


	# N is batch size; D_in is input dimension;
	# H is hidden dimension; D_out is output dimension.
	N, D_in, H, D_out = 2000, 1, 100, 1

	# Create random input and output data
	x, y  = torch.zeros((N,D_in)), torch.zeros((N,D_in))
	# x[:,0] = torch.linspace(0,4*np.pi,N)
	# y[:,0] = torch.sin(x[:,0]) + 0.25*torch.randn(D_in, N)
	x[:,0] = torch.linspace(0,4,N)
	y[:,0] = torch.clamp(torch.add(x[:,0],-2),0,10) + 0.25*torch.randn(D_in, N)


	x, y = Variable(torch.FloatTensor(x)), Variable(torch.FloatTensor(y))

	plt.figure(1)
	plt.scatter(x,y,c='r',lw=0,s=10)
	

	net = Net(D_in,H,D_out)

	learning_rate =  0.01 
	# optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
	optimizer = torch.optim.Adam( net.parameters() , lr=learning_rate)


	# train the network
	for epoch in range(1000):  # loop over the dataset multiple times

		# zero the parameter gradients
		optimizer.zero_grad()

		# forward + backward + optimize
		y_pred = net(x)

		loss = (y_pred - y).pow(2).sum()
		if epoch%100==1: print(epoch, loss.data[0])


		loss.backward()
		optimizer.step()


	# Create random input and output data
	x_test  = torch.zeros((N,D_in))
	# x_test[:,0] = torch.linspace(0,6*np.pi,N)
	x_test[:,0] = torch.linspace(-1,5,N)
	x_test = Variable(torch.FloatTensor(x_test))

	y_pred = net(x_test)

	plt.scatter(x_test,y_pred,c='b',lw=0,s=10)
	plt.show()


if __name__ == "__main__":
	
	example2()
