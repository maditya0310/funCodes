from __future__ import print_function
import patsy
import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt
from statsmodels.regression.quantile_regression import QuantReg

data = sm.datasets.engel.load_pandas().data
print(data.head())

xy = np.zeros((len(data['income']),2))
xy[:,0] = data['income']
xy[:,1] = data['foodexp']

xMatrix = np.ones((len(xy[:,0]),2))
xMatrix[:,0] = 1
xMatrix[:,1] = xy[:,0]


mod = smf.quantreg('foodexp ~ income', data)
res = mod.fit(q=.99)
print(res.summary())

quant = 0.99

import scipy.optimize

# banana = lambda x: np.sum((xy[:,1]-x[0]-x[1]*xy[:,0])*( np.where(x[0] + x[1]*xy[:,0] - xy[:,1]>=0,quant-1,quant) ))
banana = lambda x: np.sum((xy[:,1]-np.dot(xMatrix,x))*( np.where(np.dot(xMatrix,x) - xy[:,1]>=0,quant-1,quant) ))


xopt = scipy.optimize.fmin(func=banana, x0=[-1.2,1])
print(xopt)


plt.scatter(xy[:,0], xy[:,1],c='b',lw=0)
plt.xlabel('income')
plt.ylabel('food exp')

predict = xopt[0]+xopt[1]*xy[:,0]

plt.scatter(xy[:,0], predict,c='r',lw=0)

plt.show()


